import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponenteCarrosselComponent } from './componente-carrossel.component';

describe('ComponenteCarrosselComponent', () => {
  let component: ComponenteCarrosselComponent;
  let fixture: ComponentFixture<ComponenteCarrosselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponenteCarrosselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponenteCarrosselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
