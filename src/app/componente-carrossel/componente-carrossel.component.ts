import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-componente-carrossel',
  templateUrl: './componente-carrossel.component.html',
  styleUrls: ['./componente-carrossel.component.css']
})
export class ComponenteCarrosselComponent implements OnInit {
  value: number = 0;
  imagens = ['assets/gato.jpg','assets/pato.jpg','assets/sapo.jpg','assets/capivara.jpg','assets/hipopotomo.jpg','assets/rato.jpg','assets/tamandua.jpg'];

  constructor() { }

  ngOnInit(): void {
  }

}
